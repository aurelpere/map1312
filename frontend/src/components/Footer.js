import React from "react";
import {Link} from "react-router-dom";
import {Col, Container, Rpw} from "react-bootstrap";

const style = {
    textAlign: "center",
    position: "fixed",
    left: "0",
    bottom: "0",
    height: "5rem",
    width: "100%",
    display: "block"
};

const Footer = () => {
    return (

        <div style={style}>
            <footer className="footer">
                <div className="row footer">

                    <div className="row">
                        <div className="col-md-1 d-md-block d-none"/>
                        <div
                            className="col-2 col-md-1 d-flex flex-wrap justify-content-start text-md-start">
                        <span className="align-items-center"><a href="https://liberapay.com/aurelpere/donate"><img
                            className="img-fluid"
                            alt="Donner en utilisant Liberapay"
                            src="/media/donner.png"/></a></span>

                        </div>
                        <div
                            className="col-8 col-md-8 d-flex flex-wrap text-center  justify-content-center">
                            <br/>
                            <p className="text-light">© Copyright 2022-2023 <a className="text-light"
                                                                               href="https://www.matangi.dev/">Matangi.dev</a>
                            </p>
                        </div>
                        <div
                            className="col-2 col-md-2 d-flex flex-wrap justify-content-start text-md-start">
                            <a href="mailto:contact@harcelement.app"><img
                                className="img-fluid"

                                alt="envoyer un email"
                                src="/media/icon-e-mail24.png"/></a>
                        </div>
                    </div>


                </div>
            </footer>

        </div>
    );
}
export default Footer;
