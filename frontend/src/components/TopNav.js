import React, {useEffect, useState} from "react";
import {Navbar, Nav, Container, NavDropdown, Image, Row, Col, Dropdown} from "react-bootstrap";
import {Link} from "react-router-dom";
import {Transition} from "@headlessui/react";
import {Menu} from '@headlessui/react'
import {useMutation, useQueryClient} from "@tanstack/react-query";
import logout from "../functions/logout";

const TopNav = () => {
    const queryClient = useQueryClient()

    const logoutMutation = useMutation({
            mutationFn: logout,
            onSuccess: (data) => {
                queryClient.invalidateQueries(["remotetoken"])
                window.location.href = "/login";
            },
            //onError: (error) => {},
        }
    );
    return (
        <nav className="color-nav">
            <div className="row color-nav d-none d-md-block">
                <div className="row color-nav">
                    <div className="col-md-1">

                    </div>
                    <div className="col-md-9">
                        <div
                            className="justify-content-start text-md-start d-flex flex-wrap align-items-center">
                            <Link className="mx-1 px-1"
                                  to="/"><Image
                                height="50px"
                                width="125px"
                                src={"/media/logo.png"}
                                alt="Logo Harcelement.app"
                            /></Link>

                            <Link
                                className="mx-1 px-1"
                                to="/creerdossier"><Image
                                src='/media/creerdossier.png'
                                alt="Créer dossier"/></Link>
                            <Link
                                className="link-nav align-bottom mx-1 px-1"
                                to="/bilans">Bilans</Link>
                            <Link className="link-nav  mx-1 px-1 text-1rem "
                                  to="/dossier">Dossier</Link>
                            <Dropdown className="mx-1 px-1">
                                <Dropdown.Toggle variant="primary" id="nav-dropdown">
                                    Annuaire Pro
                                </Dropdown.Toggle>

                                <Dropdown.Menu>
                                    <Dropdown.Item><Link
                                        className="link-dropdown p-2"
                                        to="/annuaire">Avocats</Link></Dropdown.Item>
                                    <Dropdown.Item><Link
                                        className="link-dropdown p-2"
                                        to="/annuaire">Psychologues</Link></Dropdown.Item>
                                    <Dropdown.Item><Link
                                        className="link-dropdown p-2"
                                        to="/annuaire">Médiateurs</Link></Dropdown.Item>
                                    <Dropdown.Item><Link
                                        className="link-dropdown p-2"
                                        to="/annuaire">Syndicats</Link></Dropdown.Item>
                                    <Dropdown.Item><Link
                                        className="link-dropdown p-2"
                                        to="/annuaire">Associations</Link></Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown>
                            <Link className="link-nav mx-1 px-1"
                                  to="/forum">Forum</Link>
                        </div>
                    </div>
                    <div
                        className="col-md-2 d-flex flex-wrap align-items-center justify-content-start text-md-start">
                        {window.django.auth === "True" ? (
                            <Dropdown className="align-items-center justify-content-start text-md-start">
                                <Dropdown.Toggle variant="primary" id="nav-dropdown"
                                                 className="d-flex align-items-center">
                                    <Image src={'/media/icon-white_login.png'}
                                           alt="icone loggedin"/> </Dropdown.Toggle>

                                <Dropdown.Menu>
                                    <Dropdown.Item> <Link
                                        onClick={() => logoutMutation.mutate()}
                                        className="link-dropdown p-2"
                                        to="/">deconnecter</Link></Dropdown.Item>
                                    <Dropdown.Item><Link
                                        className="link-dropdown p-2"
                                        to="/profil">profil</Link></Dropdown.Item>


                                </Dropdown.Menu>
                            </Dropdown>


                        ) : (
                            <Dropdown className="align-items-center justify-content-start text-md-start">
                                <Dropdown.Toggle variant="primary" id="nav-dropdown"
                                                 className="d-flex align-items-center">
                                    <Image src={'/media/icon-white_login.png'}
                                           alt="icone loggedin"/>
                                </Dropdown.Toggle>

                                <Dropdown.Menu>
                                    <Dropdown.Item> <Link
                                        className="link-dropdown p-2"
                                        to="/login">se connecter</Link></Dropdown.Item>
                                    <Dropdown.Item><Link
                                        className="link-dropdown p-2"
                                        to="/inscription">s'inscrire</Link></Dropdown.Item>


                                </Dropdown.Menu>
                            </Dropdown>
                        )}
                    </div>
                </div>
            </div>

            <div className="row color-nav d-md-none d-block d-flex align-items-center">
                <div className="row">
                    <div className="col-12 ">
                        <Dropdown className="flex-shrink-0 align-items-center justify-content-start text-start">
                            <Dropdown.Toggle variant="primary" id="nav-dropdown"
                                             className="d-flex align-items-center">
                                <Image src={'/media/logo.png'}
                                       alt="menu"
                                       height="50rem"
                                       width="125rem"/>
                            </Dropdown.Toggle>

                            <Dropdown.Menu>

                                <Dropdown.Item><Link
                                    className=""
                                    to="/creerdossier"><Image
                                    src='/media/icon-creerdossier-smartphone.png'
                                    alt="Créer dossier"/></Link></Dropdown.Item>
                                <Dropdown.Item> <Link
                                    className="link-dropdown"
                                    to="/bilans">Bilans</Link></Dropdown.Item>
                                <Dropdown.Item><Link className="link-dropdown"
                                                     to="/dossier">Dossier</Link></Dropdown.Item>
                                <Dropdown.Item><Link
                                    className="link-dropdown"
                                    to="/annuaire">Avocats</Link></Dropdown.Item>
                                <Dropdown.Item><Link
                                    className="link-dropdown"
                                    to="/annuaire">Psychologues</Link></Dropdown.Item>
                                <Dropdown.Item><Link
                                    className="link-dropdown"
                                    to="/annuaire">Médiateurs</Link></Dropdown.Item>
                                <Dropdown.Item><Link
                                    className="link-dropdown"
                                    to="/annuaire">Syndicats</Link></Dropdown.Item>
                                <Dropdown.Item><Link
                                    className="link-dropdown"
                                    to="/annuaire">Associations</Link></Dropdown.Item>
                                <Dropdown.Item> <Link className="link-dropdown"
                                                      to="/forum">Forum</Link></Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                    </div>

                </div>
            </div>

        </nav>
    );
}
export default TopNav;
