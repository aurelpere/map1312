import React from "react";
import Content from "../common/Content"
import MapWrapper from "./MapWrapper";
import MapComponent from "./MapComponent";

import {Spinner} from "reactstrap";

const ErrorComponent = () => {

    const content = <div>
        une erreur s'est produite
    </div>
    return (
        <Content
            content={content}
            offset={0}
        />

    )
};
export default ErrorComponent;
