import React from "react";
import { useState } from "react";
import { useEffect } from "react";
import { useRef } from "react";
import markerImg from "../../../static/frontend/marker.png";
//import Geocode from "react-geocode";
const MapWrapper = ({
  listeAdresses,
  project,
  setProject,
}) => {
  const [trigger, setTrigger] = useState(false);
  const [mounted, setMounted] = useState(false);
  const [latlnglist,setLatlnglist]=useState([]);
  const [d,setD]=useState(0);
  const ref = useRef(null);
  let geocoder = new google.maps.Geocoder();
  let map;
  let mapOptions = {
    zoom: 11,
    center: new google.maps.LatLng("", ""),
    styles: [
      {
        featureType: "all",
        elementType: "all",
        stylers: [
          {
            visibility: "on",
          },
        ],
      },
      {
        featureType: "administrative",
        elementType: "labels.text.fill",
        stylers: [
          {
            color: "#444444",
          },
        ],
      },
      {
        featureType: "administrative.province",
        elementType: "all",
        stylers: [
          {
            visibility: "off",
          },
        ],
      },
      {
        featureType: "administrative.locality",
        elementType: "all",
        stylers: [
          {
            visibility: "on",
          },
        ],
      },
      {
        featureType: "administrative.neighborhood",
        elementType: "all",
        stylers: [
          {
            visibility: "off",
          },
        ],
      },
      {
        featureType: "administrative.land_parcel",
        elementType: "all",
        stylers: [
          {
            visibility: "off",
          },
        ],
      },
      {
        featureType: "administrative.land_parcel",
        elementType: "labels.text",
        stylers: [
          {
            visibility: "off",
          },
        ],
      },
      {
        featureType: "landscape",
        elementType: "all",
        stylers: [
          {
            color: "#f2f2f2",
          },
        ],
      },
      {
        featureType: "landscape.man_made",
        elementType: "all",
        stylers: [
          {
            visibility: "simplified",
          },
        ],
      },
      {
        featureType: "poi",
        elementType: "all",
        stylers: [
          {
            visibility: "off",
          },
          {
            color: "#cee9de",
          },
          {
            saturation: "2",
          },
          {
            weight: "0.80",
          },
        ],
      },
      {
        featureType: "poi.attraction",
        elementType: "geometry.fill",
        stylers: [
          {
            visibility: "off",
          },
        ],
      },
      {
        featureType: "poi.park",
        elementType: "all",
        stylers: [
          {
            visibility: "on",
          },
        ],
      },
      {
        featureType: "road",
        elementType: "all",
        stylers: [
          {
            saturation: 0,
          },
          {
            lightness: 45,
          },
        ],
      },
      {
        featureType: "road.highway",
        elementType: "all",
        stylers: [
          {
            visibility: "simplified",
          },
        ],
      },
      {
        featureType: "road.highway",
        elementType: "geometry.fill",
        stylers: [
          {
            visibility: "on",
          },
          {
            color: "#fffff",
          },
        ],
      },
      {
        featureType: "road.highway",
        elementType: "labels.text",
        stylers: [
          {
            visibility: "off",
          },
        ],
      },
      {
        featureType: "road.highway",
        elementType: "labels.icon",
        stylers: [
          {
            hue: "#ff0000",
          },
          {
            visibility: "off",
          },
        ],
      },
      {
        featureType: "road.highway.controlled_access",
        elementType: "labels.text",
        stylers: [
          {
            visibility: "simplified",
          },
        ],
      },
      {
        featureType: "road.highway.controlled_access",
        elementType: "labels.icon",
        stylers: [
          {
            visibility: "on",
          },
          {
            hue: "#0064ff",
          },
          {
            gamma: "1.44",
          },
          {
            lightness: "-3",
          },
          {
            weight: "1.69",
          },
        ],
      },
      {
        featureType: "road.arterial",
        elementType: "all",
        stylers: [
          {
            visibility: "on",
          },
        ],
      },
      {
        featureType: "road.arterial",
        elementType: "labels.text",
        stylers: [
          {
            visibility: "off",
          },
        ],
      },
      {
        featureType: "road.arterial",
        elementType: "labels.icon",
        stylers: [
          {
            visibility: "off",
          },
        ],
      },
      {
        featureType: "road.local",
        elementType: "all",
        stylers: [
          {
            visibility: "on",
          },
        ],
      },
      {
        featureType: "road.local",
        elementType: "labels.text",
        stylers: [
          {
            visibility: "simplified",
          },
          {
            weight: "0.31",
          },
          {
            gamma: "1.43",
          },
          {
            lightness: "-5",
          },
          {
            saturation: "-22",
          },
        ],
      },
      {
        featureType: "transit",
        elementType: "all",
        stylers: [
          {
            visibility: "off",
          },
        ],
      },
      {
        featureType: "transit.line",
        elementType: "all",
        stylers: [
          {
            visibility: "on",
          },
          {
            hue: "#ff0000",
          },
        ],
      },
      {
        featureType: "transit.station.airport",
        elementType: "all",
        stylers: [
          {
            visibility: "simplified",
          },
          {
            hue: "#ff0045",
          },
        ],
      },
      {
        featureType: "transit.station.bus",
        elementType: "all",
        stylers: [
          {
            visibility: "on",
          },
          {
            hue: "#00d1ff",
          },
        ],
      },
      {
        featureType: "transit.station.bus",
        elementType: "labels.text",
        stylers: [
          {
            visibility: "simplified",
          },
        ],
      },
      {
        featureType: "transit.station.rail",
        elementType: "all",
        stylers: [
          {
            visibility: "simplified",
          },
          {
            hue: "#00cbff",
          },
        ],
      },
      {
        featureType: "transit.station.rail",
        elementType: "labels.text",
        stylers: [
          {
            visibility: "simplified",
          },
        ],
      },
      {
        featureType: "water",
        elementType: "all",
        stylers: [
          {
            color: "#46bcec",
          },
          {
            visibility: "on",
          },
        ],
      },
      {
        featureType: "water",
        elementType: "geometry.fill",
        stylers: [
          {
            weight: "1.61",
          },
          {
            color: "#AECDCD",
          },
          {
            visibility: "on",
          },
        ],
      },
    ],
  };


function findFurthestPoints(points) {
  let maxDistance = 0;
  let pointA = null;
  let pointB = null;

  for (let i = 0; i < points.length - 1; i++) {
    for (let j = i + 1; j < points.length; j++) {
      const distance = getDistanceFromLatLonInKm(points[j][0],points[j][1],points[i][0],points[i][1])
      if (distance > maxDistance) {
        maxDistance = distance;
        pointA = points[i];
        pointB = points[j];
      }
    }
  }

  return [pointA, pointB];
}
  // Choisir un point arbitraire


function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2-lat1);  // deg2rad below
  var dLon = deg2rad(lon2-lon1); 
  var a = 
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
    Math.sin(dLon/2) * Math.sin(dLon/2)
    ; 
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
  var d = R * c; // Distance in km
  return d;
}

function deg2rad(deg) {
  return deg * (Math.PI/180)
}
  const compute = () => {
  map = new google.maps.Map(ref.current, mapOptions);
  ////console.log("latlnglist");
  ////console.log(latlnglist);
  //const circle=calculateMinimumBoundingCircle(latlnglist);
  const latlist=latlnglist.map(function getlatitude (item) {
  return item[0];
});
  const lnglist=latlnglist.map(function getlongitude (item) {
  return item[1];
});
  const minlat = Math.min.apply(Math,latlist);
  const minlng = Math.min.apply(Math,lnglist);
  const maxlat = Math.max.apply(Math,latlist);
  const maxlng = Math.max.apply(Math,lnglist);
  const AB=findFurthestPoints(latlnglist);
  const latAB=AB.map(function getlatitude (item) {
  return item[0];
});
  const lngAB=AB.map(function getlongitude (item) {
  return item[1];
});
  const ABminlat = Math.min.apply(Math,latAB);
  const ABminlng = Math.min.apply(Math,lngAB);
  const ABmaxlat = Math.max.apply(Math,latAB);
  const ABmaxlng = Math.max.apply(Math,lngAB); 

  const centerlat=ABminlat+(ABmaxlat-ABminlat)/2;
  const centerlng=ABminlng+(ABmaxlng-ABminlng)/2;

  const center = new google.maps.LatLng(centerlat,centerlng);
  const diameter=getDistanceFromLatLonInKm(ABminlat,ABminlng,ABmaxlat,ABmaxlng);
  setD(diameter);
  ////console.log('d');
  ////console.log(d);
  const Latlngsw = new google.maps.LatLng(minlat-0.1, minlng-0.1);
  ////console.log(Latlngsw);
  const Latlngne = new google.maps.LatLng(maxlat+0.1, maxlng+0.1);
 latlnglist.forEach((item) => {
  let latlng = new google.maps.LatLng(item[0], item[1]);
  let marker = new google.maps.Marker({
      position: { lat: item[0], lng: item[1] },
      label: item[2],
      map,
      icon: markerImg,
    });
    if (latlnglist.length===1) {
    map.setCenter(latlng);
    }
    //
    marker.setMap(map);
    });
    
    if (latlnglist.length>1) {
    let latlngbound = new google.maps.LatLngBounds(Latlngsw, Latlngne);
    map.fitBounds(latlngbound);
    const circleoptions={
      strokeColor: "#FF0000",
      strokeOpacity: 0.2,
      strokeWeight: 1,
      fillColor: "#FF0000",
      fillOpacity: 0.4,
      map,
      center: center, //circle.center
      radius: d*1000/2, //circle.radius,
    };
    const circle = new google.maps.Circle(circleoptions);
    }
    }; 
    
    const initialize = () => {
  //documentation:
  //https://developers.google.com/maps/documentation/javascript/reference?hl=fr
  //https://developers.google.com/maps/documentation/javascript/reference/map?hl=fr
    let geocoder = new google.maps.Geocoder();
    setLatlnglist((latlnglist) => {
    return [];
    });
    listeAdresses.forEach((item) => {
    ////console.log(item);
    const latitude = item.latlng[0];
    ////console.log("latitude");
    ////console.log(latitude);
    const longitude = item.latlng[1];
    ////console.log("longitude");
    ////console.log(longitude);
    setLatlnglist((latlnglist) => {
      const newLatLng = [latitude, longitude, item.name];
      if (!latlnglist.some(([lat, lng]) => lat === latitude && lng === longitude)) {
        // Only add the newLatLng if it doesn't already exist in the array
        return [...latlnglist, newLatLng];
      }
      return latlnglist;
    });
    setTrigger((trigger)=>{return !trigger});
});
});
};
      useEffect(() => {
        if (listeAdresses.length>=1) {
        initialize();
        }
      }, [listeAdresses]);
      
      useEffect(() => {
        if (mounted) {
          compute();
         } else {
           setMounted(true);
          }
        }, [trigger]);

  return (
  <>
    <div
      ref={ref}
      id="map"
      className="h-full rounded-tr-2xl lg:rounded-tr-none rounded-tl-2xl lg:rounded-bl-2xl"
    />
    <div>
      <p>le diametre du cercle incluant tous les projets est de {d.toFixed(2)} kilometres</p>
    </div>
    </>
  );
};

export default MapWrapper;
