import {createBrowserRouter, Route, Routes, Switch, redirect, RouterProvider, BrowserRouter} from "react-router-dom";

import Accueil from "./Accueil";

import {useStore} from "../store/store";
import {useQuery, useQueryClient} from "@tanstack/react-query";
import {getTokenDetail} from "../functions/getTokenDetail";
import TopNav from "./TopNav";
import Footer from "./Footer";


function App() {

    //useEffect(() => {
    //    console.log(user); // Log the updated user state whenever it changes
    //}, [user]);
    const routes =
        [

            {
                path: "/alive",
                element: <Accueil/>
            },
        ];
    const router = createBrowserRouter(routes);

    return (
        <BrowserRouter>
            {/* <TopNav/>Render the TopNav component */}
            <div className="content-container">
                <Routes>
                    {/* Render the route components */}
                    {routes.map((route, index) => (
                        <Route
                            key={index}
                            path={route.path}
                            element={route.element}
                        />
                    ))}
                </Routes>
            </div>
            {/* <Footer/>  Render the Footer component */}
        </BrowserRouter>
    );

}

export default App;
