import React from "react";
import {useState} from "react";
import {useEffect} from "react";
import {useRef} from "react";
import markerImg from "../../../static/frontend/marker.png";
import H2 from "../common/titles/H2";
//import Geocode from "react-geocode";
const MapWrapperColl = ({
                            listeAdresses
                        }) => {
    const [project, setProject] = useState(null);
    const [d, setD] = useState(0);
    const ref = useRef(null);
    let geocoder = new google.maps.Geocoder();
    let map;
    let mapOptions = {
        zoom: 11,
        center: new google.maps.LatLng("", ""),
        styles: [
            {
                featureType: "all",
                elementType: "all",
                stylers: [
                    {
                        visibility: "on",
                    },
                ],
            },
            {
                featureType: "administrative",
                elementType: "labels.text.fill",
                stylers: [
                    {
                        color: "#444444",
                    },
                ],
            },
            {
                featureType: "administrative.province",
                elementType: "all",
                stylers: [
                    {
                        visibility: "off",
                    },
                ],
            },
            {
                featureType: "administrative.locality",
                elementType: "all",
                stylers: [
                    {
                        visibility: "on",
                    },
                ],
            },
            {
                featureType: "administrative.neighborhood",
                elementType: "all",
                stylers: [
                    {
                        visibility: "off",
                    },
                ],
            },
            {
                featureType: "administrative.land_parcel",
                elementType: "all",
                stylers: [
                    {
                        visibility: "off",
                    },
                ],
            },
            {
                featureType: "administrative.land_parcel",
                elementType: "labels.text",
                stylers: [
                    {
                        visibility: "off",
                    },
                ],
            },
            {
                featureType: "landscape",
                elementType: "all",
                stylers: [
                    {
                        color: "#f2f2f2",
                    },
                ],
            },
            {
                featureType: "landscape.man_made",
                elementType: "all",
                stylers: [
                    {
                        visibility: "simplified",
                    },
                ],
            },
            {
                featureType: "poi",
                elementType: "all",
                stylers: [
                    {
                        visibility: "off",
                    },
                    {
                        color: "#cee9de",
                    },
                    {
                        saturation: "2",
                    },
                    {
                        weight: "0.80",
                    },
                ],
            },
            {
                featureType: "poi.attraction",
                elementType: "geometry.fill",
                stylers: [
                    {
                        visibility: "off",
                    },
                ],
            },
            {
                featureType: "poi.park",
                elementType: "all",
                stylers: [
                    {
                        visibility: "on",
                    },
                ],
            },
            {
                featureType: "road",
                elementType: "all",
                stylers: [
                    {
                        saturation: 0,
                    },
                    {
                        lightness: 45,
                    },
                ],
            },
            {
                featureType: "road.highway",
                elementType: "all",
                stylers: [
                    {
                        visibility: "simplified",
                    },
                ],
            },
            {
                featureType: "road.highway",
                elementType: "geometry.fill",
                stylers: [
                    {
                        visibility: "on",
                    },
                    {
                        color: "#fffff",
                    },
                ],
            },
            {
                featureType: "road.highway",
                elementType: "labels.text",
                stylers: [
                    {
                        visibility: "off",
                    },
                ],
            },
            {
                featureType: "road.highway",
                elementType: "labels.icon",
                stylers: [
                    {
                        hue: "#ff0000",
                    },
                    {
                        visibility: "off",
                    },
                ],
            },
            {
                featureType: "road.highway.controlled_access",
                elementType: "labels.text",
                stylers: [
                    {
                        visibility: "simplified",
                    },
                ],
            },
            {
                featureType: "road.highway.controlled_access",
                elementType: "labels.icon",
                stylers: [
                    {
                        visibility: "on",
                    },
                    {
                        hue: "#0064ff",
                    },
                    {
                        gamma: "1.44",
                    },
                    {
                        lightness: "-3",
                    },
                    {
                        weight: "1.69",
                    },
                ],
            },
            {
                featureType: "road.arterial",
                elementType: "all",
                stylers: [
                    {
                        visibility: "on",
                    },
                ],
            },
            {
                featureType: "road.arterial",
                elementType: "labels.text",
                stylers: [
                    {
                        visibility: "off",
                    },
                ],
            },
            {
                featureType: "road.arterial",
                elementType: "labels.icon",
                stylers: [
                    {
                        visibility: "off",
                    },
                ],
            },
            {
                featureType: "road.local",
                elementType: "all",
                stylers: [
                    {
                        visibility: "on",
                    },
                ],
            },
            {
                featureType: "road.local",
                elementType: "labels.text",
                stylers: [
                    {
                        visibility: "simplified",
                    },
                    {
                        weight: "0.31",
                    },
                    {
                        gamma: "1.43",
                    },
                    {
                        lightness: "-5",
                    },
                    {
                        saturation: "-22",
                    },
                ],
            },
            {
                featureType: "transit",
                elementType: "all",
                stylers: [
                    {
                        visibility: "off",
                    },
                ],
            },
            {
                featureType: "transit.line",
                elementType: "all",
                stylers: [
                    {
                        visibility: "on",
                    },
                    {
                        hue: "#ff0000",
                    },
                ],
            },
            {
                featureType: "transit.station.airport",
                elementType: "all",
                stylers: [
                    {
                        visibility: "simplified",
                    },
                    {
                        hue: "#ff0045",
                    },
                ],
            },
            {
                featureType: "transit.station.bus",
                elementType: "all",
                stylers: [
                    {
                        visibility: "on",
                    },
                    {
                        hue: "#00d1ff",
                    },
                ],
            },
            {
                featureType: "transit.station.bus",
                elementType: "labels.text",
                stylers: [
                    {
                        visibility: "simplified",
                    },
                ],
            },
            {
                featureType: "transit.station.rail",
                elementType: "all",
                stylers: [
                    {
                        visibility: "simplified",
                    },
                    {
                        hue: "#00cbff",
                    },
                ],
            },
            {
                featureType: "transit.station.rail",
                elementType: "labels.text",
                stylers: [
                    {
                        visibility: "simplified",
                    },
                ],
            },
            {
                featureType: "water",
                elementType: "all",
                stylers: [
                    {
                        color: "#46bcec",
                    },
                    {
                        visibility: "on",
                    },
                ],
            },
            {
                featureType: "water",
                elementType: "geometry.fill",
                stylers: [
                    {
                        weight: "1.61",
                    },
                    {
                        color: "#AECDCD",
                    },
                    {
                        visibility: "on",
                    },
                ],
            },
        ],
    };

    function findFurthestPoints(points) {
        let maxDistance = 0;
        let pointA = null;
        let pointB = null;
        if (points.length > 2) {
            for (let i = 0; i < points.length - 1; i++) {
                for (let j = i + 1; j < points.length; j++) {
                    const distance = getDistanceFromLatLonInKm(points[j].latitude, points[j].longitude, points[i].latitude, points[i].longitude)
                    ////console.log("distance");
                    ////console.log(distance);
                    if (distance > maxDistance) {
                        maxDistance = distance;
                        pointA = [points[i].latitude, points[i].longitude];
                        pointB = [points[j].latitude, points[j].longitude];
                    }
                }
            }
            return [pointA, pointB];
        } else if (points.length === 2) {
            return [[points[0].latitude, points[0].longitude], [points[1].latitude, points[1].longitude]]
        } else {
            return [[0, 0], [1, 1]]
        }
    }

    // Choisir un point arbitraire


    function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {

        var R = 6371; // Radius of the earth in km
        var dLat = deg2rad(lat2 - lat1);  // deg2rad below
        var dLon = deg2rad(lon2 - lon1);
        var a =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2)
        ;
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var diameter = R * c; // Distance in km
        return diameter;
    }

    function deg2rad(deg) {
        return deg * (Math.PI / 180)
    }

    function displayProjectInfos(pk) {
        var foundProject = listeAdresses.find(function (item) {
            return item.id === parseInt(pk);
        });
        setProject(foundProject.project);
    }

    const compute = () => {
        map = new google.maps.Map(ref.current, mapOptions);
        ////console.log("listeAdresses");
        ////console.log(listeAdresses);
        //const circle=calculateMinimumBoundingCircle(latlnglist);
        const latlist = listeAdresses.map(function getlatitude(item) {
            return item.latitude;
        });
        const lnglist = listeAdresses.map(function getlongitude(item) {
            return item.longitude;
        });
        const minlat = Math.min.apply(Math, latlist);
        const minlng = Math.min.apply(Math, lnglist);
        const maxlat = Math.max.apply(Math, latlist);
        const maxlng = Math.max.apply(Math, lnglist);
        const AB = findFurthestPoints(listeAdresses);
        ////console.log(AB);
        const latAB = AB.map(function getlatitude(item) {
            return item[0];
        });
        const lngAB = AB.map(function getlongitude(item) {
            return item[1];
        });
        const ABminlat = Math.min.apply(Math, latAB);
        const ABminlng = Math.min.apply(Math, lngAB);
        const ABmaxlat = Math.max.apply(Math, latAB);
        const ABmaxlng = Math.max.apply(Math, lngAB);

        const centerlat = ABminlat + (ABmaxlat - ABminlat) / 2;
        const centerlng = ABminlng + (ABmaxlng - ABminlng) / 2;

        const center = new google.maps.LatLng(centerlat, centerlng);
        const diameter = getDistanceFromLatLonInKm(ABminlat, ABminlng, ABmaxlat, ABmaxlng);
        setD(d => {
            return diameter
        });

        ////console.log('d');
        ////console.log(d);
        const Latlngsw = new google.maps.LatLng(minlat - 0.01, minlng - 0.01);
        ////console.log(Latlngsw);
        const Latlngne = new google.maps.LatLng(maxlat + 0.01, maxlng + 0.01);
        listeAdresses.forEach((item) => {
            ////console.log(item);
            let latlng = new google.maps.LatLng(item.latitude, item.longitude);
            let marker = new google.maps.Marker({
                position: {lat: item.latitude, lng: item.longitude},
                title: item.id.toString(),
                label: item.name,
                map,
                icon: markerImg,
                clickable: true
            })

            if (listeAdresses.length === 1) {
                map.setCenter(latlng);
                setD(0);
            }

            marker.setMap(map);
            marker.setDraggable(true);
            marker.addListener('click', function () {
                // fonction à exécuter lorsque le marqueur est cliqué
                var pk = marker.getTitle();
                displayProjectInfos(pk);
            });
            var pk = marker.getTitle();
            displayProjectInfos(pk);
        });

        if (listeAdresses.length > 1) {
            let latlngbound = new google.maps.LatLngBounds(Latlngsw, Latlngne);
            map.fitBounds(latlngbound);
            const circleoptions = {
                strokeColor: "#FF0000",
                strokeOpacity: 0.2,
                strokeWeight: 1,
                fillColor: "#FF0000",
                fillOpacity: 0.7,
                map,
                center: center, //circle.center
                radius: diameter * 1000 / 2, //circle.radius,
            };
            const circle = new google.maps.Circle(circleoptions);
        }
    };

    useEffect(() => {
        if (listeAdresses.length >= 1) {
            compute();
        }
    }, [listeAdresses]);


    //////console.log("project");
    //////console.log(project);
    return (
        <div className="flex flex-col mb-20 lg:flex-row">
            <div className="w-full h-64 lg:h-auto lg:w-1/2">
                {/* Carte */}
                <div
                    ref={ref}
                    id="map"
                    className="h-full rounded-tr-2xl lg:rounded-tr-none rounded-tl-2xl lg:rounded-bl-2xl"
                />
                <div>
                    <p>Le diamètre du cercle des deux projets les plus éloignés est de {d.toFixed(2)} km</p>
                </div>
            </div>
            {/* Description */}
            <div
                className="flex flex-col lg:flex-row w-full p-2 space-x-8 bg-white lg:w-1/2 md:p-8 lg:rounded-tr-2xl rounded-br-2xl rounded-bl-2xl lg:rounded-bl-none"
                style={{minHeight: '200px'}}>
                {/* Left */}
                <div className="w-full lg:w-1/2">
                    {project ? (
                        <>
                            <H2 id="coordonnees" text="Coordonnées"/>
                            <h3 className="mt-3 font-semibold text-18px xl:text-20px">Nom</h3>
                            <p>{project.nom}</p>
                            <h3 className="mt-3 font-semibold text-18px xl:text-20px">Adresse</h3>
                            <p>{project.adresse_installation_api}</p>
                            <h3 className="mt-3 font-semibold text-20px">Jours ouvrés</h3>
                            <p className="flex flex-wrap space-x-1">
                                {project.liste_jour_ouvres_display}
                            </p>
                        </>
                    ) : null}
                </div>
                {/* Right */}
                <div className="w-full lg:w-1/2">
                    {project ? (
                        <>
                            <h2 className="font-normal text-26px xl:text-28px">Informations</h2>
                            <h3 className="mt-3 font-semibold text-18px xl:text-20px">PRM</h3>
                            <p>{project.prm}</p>
                            <h3 className="mt-3 font-semibold text-18px xl:text-20px">
                                Type de bâtiment
                            </h3>
                            <p>{project.sous_categorie_batiment_display}</p>
                            <h3 className="mt-3 font-semibold text-18px xl:text-20px">
                                Puissance souscrite
                            </h3>
                            <p>{project.puissance_souscrite}</p>
                            <h3 className="mt-3 font-semibold text-18px xl:text-20px">
                                Type de compteur
                            </h3>
                            <p>{project.type_compteur}</p>
                        </>
                    ) : null}
                </div>
            </div>
        </div>


    );
};

export default MapWrapperColl;
