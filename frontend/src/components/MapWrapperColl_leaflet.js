import React from "react";
import {useState} from "react";
import {useEffect} from "react";
import {useRef} from "react";
import L from "leaflet";
import * as Nominatim from "nominatim-browser";
import "leaflet/dist/leaflet.css";
import markerImg from "../../../static/frontend/marker.png";
//import Geocode from "react-geocode";
const MapWrapperColl = ({
                            listeAdresses,
                            project,
                            setProject,
                        }) => {
    const [trigger, setTrigger] = useState(false);
    const [mounted, setMounted] = useState(false);
    const [latlnglist, setLatlnglist] = useState([]);
    const [map, setMap] = useState(null);
    const [myLayerGroup, setMyLayerGroup] = useState(null);
    const [d, setD] = useState(0);
    const ref = useRef(null);

    function calculateMinimumBoundingCircle(points) {
        // Vérifier que la liste de points n'est pas vide
        if (points.length === 0) {
            return {center: null, radius: 0};
        }
        if (points.length === 1) {
            return {center: [points[0][0], points[0][1]], radius: 0};
        }
        // Initialiser le cercle minimum couvrant avec les deux premiers points
        let C = circleFromTwoPoints(points[0], points[1]);

        // Itérer sur tous les autres points
        for (let i = 2; i < points.length; i++) {
            const p = points[i];

            // Si le point est déjà dans le cercle, passer au point suivant
            if (isInsideCircle(C, p)) {
                continue;
            }

            // Trouver le cercle minimum couvrant pour le sous-ensemble de points qui inclut ce point et tous les points déjà dans le cercle minimum couvrant
            C = minimumBoundingCircleWithPoint(points.slice(0, i + 1), p);
        }

        // Retourner le centre et le rayon du cercle minimum couvrant
        return {center: C.center, radius: C.radius};
    }

    function distanceBetweenPoints(p1, p2) {
        const dx = p1[0] - p2[0];
        const dy = p1[1] - p2[1];
        return Math.sqrt(dx * dx + dy * dy);
    }

    function circleFromTwoPoints(p1, p2) {
        const center = {
            x: (p1[0] + p2[0]) / 2,
            y: (p1[1] + p2[1]) / 2,
        };
        const radius = distanceBetweenPoints(p1, p2) / 2;
        return {center, radius};
    }

    function isInsideCircle(C, p) {
        const dx = C.center[0] - p[0];
        const dy = C.center[1] - p[1];
        const distanceSquared = dx * dx + dy * dy;
        return distanceSquared <= C.radius * C.radius;
    }

    function minimumBoundingCircleWithPoint(points, p) {
        // Si la liste de points est vide ou ne contient qu'un seul point, renvoyer un cercle centré sur ce point avec un rayon nul
        if (points.length === 0) {
            return {center: p, radius: 0};
        }

        if (points.length === 1) {
            return circleFromTwoPoints(points[0], p);
        }

        // Vérifier si le point est déjà dans le cercle minimum couvrant
        const C = minimumBoundingCircle(points);
        if (isInsideCircle(C, p)) {
            return C;
        }

        // Trouver le cercle minimum couvrant pour le sous-ensemble de points qui inclut le dernier point ajouté et tous les points déjà dans le cercle minimum couvrant
        const pointsWithP = points.concat(p);
        return minimumBoundingCircleWithPoint(pointsWithP.slice(0, pointsWithP.length - 1), p);
    }

    function minimumBoundingCircle(points) {
        // Si la liste de points est vide ou ne contient qu'un seul point, renvoyer un cercle avec un rayon infini
        if (points.length === 0) {
            return {center: null, radius: Number.POSITIVE_INFINITY};
        }

        if (points.length === 1) {
            return {center: points[0], radius: 0};
        }

        if (points.length === 2) {
            return circleFromTwoPoints(points[0], points[1]);
        }
    }

    // Choisir un point arbitraire


    function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
        var R = 6371; // Radius of the earth in km
        var dLat = deg2rad(lat2 - lat1);  // deg2rad below
        var dLon = deg2rad(lon2 - lon1);
        var a =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2)
        ;
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c; // Distance in km
        return d;
    }

    function deg2rad(deg) {
        return deg * (Math.PI / 180)
    }

    const compute = () => {
        //map = L.map("map");
        ////console.log("latlnglist");
        ////console.log(latlnglist);
        //const circle=calculateMinimumBoundingCircle(latlnglist);
        const latlist = latlnglist.map(function getlatitude(item) {
            return item[0];
        });
        const lnglist = latlnglist.map(function getlongitude(item) {
            return item[1];
        });
        const minlat = Math.min.apply(Math, latlist);
        const minlng = Math.min.apply(Math, lnglist);
        const maxlat = Math.max.apply(Math, latlist);
        const maxlng = Math.max.apply(Math, lnglist);
        const centerlat = minlat + (maxlat - minlat) / 2;
        const centerlng = minlng + (maxlng - minlng) / 2;
        const center = [centerlat, centerlng];
        const diameter = getDistanceFromLatLonInKm(minlat, minlng, maxlat, maxlng);
        setD(diameter);
        ////console.log('d');
        ////console.log(d);
        const Latlngsw = [minlat, minlng];
        ////console.log(Latlngsw);
        const Latlngne = [maxlat, maxlng];
        myLayerGroup.clearLayers()
        latlnglist.forEach((item) => {
            let latlng = [item[0], item[1]];
            let marker = L.marker([item[0], item[1]], {
                icon: L.icon({
                    iconUrl: markerImg,
                    iconSize: [38, 38],
                    iconAnchor: [22, 37],
                    popupAnchor: [-3, -36],
                }),
            }).addTo(myLayerGroup);
            if (latlnglist.length === 1) {
                map.setView(latlng);
            }
            //
        });

        if (latlnglist.length > 1) {
            let latlngbound = [Latlngsw, Latlngne];
            map.fitBounds(latlngbound);
            const circleoptions = {
                color: "#FF0000",
                weight: 1,
                fillColor: "#FF0000",
                fillOpacity: 0.4,
            };
            //const circle = L.circle(center, {...circleoptions,radius: diameter * 1000 / 2,}).addTo(map);
        }
    };

    const initialize = () => {
        //documentation:
        //https://www.npmjs.com/package/nominatim-browser
        setLatlnglist((latlnglist) => {
            return [];
        });
        listeAdresses.forEach((item) => {
            ////console.log(item);
            Nominatim.geocode({
                q: item.fullAddress,
                addressdetails: true
            })
                .then((results) => {
                    ////console.log(results);
                    const latitude = results[0].lat;
                    const longitude = results[0].lon;
                    ////console.log("latitude");
                    ////console.log(latitude);
                    ////console.log("longitude");
                    ////console.log(longitude);
                    setLatlnglist((latlnglist) => {
                        const newLatLng = [latitude, longitude, item.name];
                        if (!latlnglist.some(([lat, lng]) => lat === latitude && lng === longitude)) {
                            // Only add the newLatLng if it doesn't already exist in the array
                            return [...latlnglist, newLatLng];
                        }
                        return latlnglist;
                    });
                    setTrigger((trigger) => {
                        return !trigger
                    });
                })
                .catch((error) => {
                    console.error(error);
                });

        });
    }
    useEffect(() => {
        // create the map instance
        const mapInstance = L.map(ref.current, {
            center: [45.86102, 1.29156],
            zoom: 17
        });
        setMap(map => {
            return mapInstance
        });
        // add the tile layer
        const bounds = [[45.85, 1.28], [45.86, 1.29]]
        let myLayerGroup = L.layerGroup().addTo(mapInstance);
        L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png', //'https://stamen-tiles.a.ssl.fastly.net/toner/{z}/{x}/{y}.png', //'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
            {
                maxZoom: 18,
                //bounds: bounds,
                attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
            }).addTo(mapInstance);
        setMyLayerGroup(myLayerGroup);
    }, []);

    useEffect(() => {
        if (listeAdresses.length >= 1) {
            initialize();
        }
    }, [listeAdresses]);

    useEffect(() => {
        if (mounted) {
            compute();
        } else {
            setMounted(true);
        }
    }, [trigger]);

    return (
        <>
            <div
                ref={ref}
                id="map"
                className="h-full rounded-tr-2xl lg:rounded-tr-none rounded-tl-2xl lg:rounded-bl-2xl"
            />

            <div>
                <p>le diametre du cercle incluant tous les projets est de {d.toFixed(2)} kilometres</p>
            </div>
        </>
    );
};

export default MapWrapperColl;
