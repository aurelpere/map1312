import axios from "axios";
import {getCookie} from "./getCookie";

export const postmap = async (payload, sup, inf,ipAddress) => {
    try {
        //const ipAddressResponse = await axios.get('https://api.ipify.org');
        //const ipAddress = ipAddressResponse.data;
        //const ipAddress = "999.999.999.999"
        const mapResponse = await axios.post(`${process.env.REACT_APP_BASE_URL}/nontiba/`,
            {
                F3JZOKwYkPw: payload._northEast.lat - 0.00000000000001,
                jSLzYIxVTHT: payload._southWest.lng + 0.00000000000001,
                mAXE7SsolyO: payload._southWest.lng - 0.00000000000001,
                vj6Jv2xLZ6u: payload._southWest.lat,
                dnfrsCgOuxE: payload._southWest.lat + 0.00000000000001,
                l3ikMVqonbe: payload._southWest.lng - 0.00000000000001,
                PWocCfzRbJT: payload._northEast.lat,
                Yzo3rwrff20: payload._northEast.lat + 0.00000000000001,
                InIG2oCBgYX: payload._northEast.lng,
                uJiGWkF75B6: payload._southWest.lng,
                A4nW8Mq24Ff: payload._southWest.lat - 0.00000000000001,
                sup: sup,
                inf: inf
            },
            {
                headers: {
                    'contentType': 'application/json',
                    'S3WE6MqjhGzVJooN': getCookie("csrftoken"),
                    'X-Forwarded-For': ipAddress,
                }
            },
        );

        return mapResponse.data;
    } catch (error) {
        console.error('An error occurred:', error);
        throw error; // Rethrow the error to be handled by the caller
    }
}

export default postmap;

