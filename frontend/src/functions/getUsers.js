import axios from "axios";

const getUsers = (authToken, params) => {
  if (process.env.REACT_APP_TOKEN) {
    authToken = process.env.REACT_APP_TOKEN;
  }
  return axios.get(
    `${process.env.REACT_APP_BASE_URL}/api/users`,
    { params },
    {
      headers: {
        Authorization: `Token ${authToken}`,
      },
    }
  );
};

export default getUsers;
