import axios from "axios";
import {getCookie} from "./getCookie";

export const logout = async (auth_token) => {
    try {
        const ipAddressResponse = await axios.get('https://api.ipify.org');
        const ipAddress = ipAddressResponse.data;

        const logoutResponse = axios.get(`${process.env.REACT_APP_BASE_URL}/logout/`, {
            undefined,
            headers: {
                'S3WE6MqjhGzVJooN': getCookie("csrftoken"),
                'X-Forwarded-For': ipAddress,
            },
        });

        return logoutResponse; // Return the actual data you want
    } catch (error) {
        console.error('An error occurred:', error);
        throw error; // Rethrow the error to be handled by the caller
    }
}

export default logout;
