import axios from "axios";
import {getCookie} from "./getCookie";

export const confirmsignup = async (payload) => {
    try {
        const ipAddressResponse = await axios.get('https://api.ipify.org');
        const ipAddress = ipAddressResponse.data;

        const confirmsignupResponse = await axios.post(`${process.env.REACT_APP_BASE_URL}/ik8LSvOEwY8neMH/`,
            {
                token: payload.token,
                uid: payload.uid,
            },
            {
                headers: {
                    'contentType': 'application/json',
                    'S3WE6MqjhGzVJooN': getCookie("csrftoken"),
                    'X-Forwarded-For': ipAddress,
                }
            },
        );

        return confirmsignupResponse.data;
    } catch (error) {
        console.error('An error occurred:', error);
        throw error; // Rethrow the error to be handled by the caller
    }
}

export default confirmsignup;

