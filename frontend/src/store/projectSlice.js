import { createSlice } from "@reduxjs/toolkit";

export const projectSlice = createSlice({
  name: "project",
  initialState: {},
  reducers: {
    setProjectDetails: (state, action) => {
      return (state = { ...action.payload });
    },
  },
});

export const { setProjectDetails, removeProject } = projectSlice.actions;

export default projectSlice.reducer;
