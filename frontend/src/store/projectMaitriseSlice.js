import { createSlice } from "@reduxjs/toolkit";

const initialState = { responseStatut: 100 };

export const projectMaitriseSlice = createSlice({
  name: "projectMaitrise",
  initialState: initialState,
  reducers: {
    setProjectMaitriseDetails: (state, action) => {
      return { ...state, data: action.payload };
    },
    removeProjectMaitriseDetails: (state, action) => {
      return initialState;
    },

    setProjectMaitriseStatut: (state, action) => {
      return { ...state, responseStatut: action.payload };
    },
  },
});

export const {
  setProjectMaitriseDetails,
  removeProjectMaitriseDetails,
  setProjectMaitriseStatut,
} = projectMaitriseSlice.actions;

export default projectMaitriseSlice.reducer;
