import React from "react";

const Content = (props) => {
    //contenant par défaut 9 colonnes en web et 12 en mobile
    let webclass = "col-md-9 " + `offset-${props.offset + 1}`
    let mobclass = "col-11"
    if (props.offset !== 0) {
        mobclass = "col-11 " + `offset-${props.offset}`
    }
    if (props.width !== undefined) {
        webclass = "col-md-" + `${props.width}` + " " + `offset-${props.offset + 1}`
        mobclass = "col-" + `${props.width}`
        if (props.offset !== 0) {
            mobclass = "col-" + `${props.width}` + " " + `offset-${props.offset}`
        }
    }
    return (
        <div className="container-fluid">
            <br/>
            <div className="row d-none d-md-block mt-2">
                <div className="col-md-1"></div>

                <div className={webclass}>
                    {props.content}
                </div>
                <div className="col-md-2">
                </div>
            </div>
            <div className="row d-md-none d-block mt-1">

                <div className={mobclass}>
                    {props.content}
                </div>
                <div className="col-1">
                </div>

            </div>
            <br/>
            <br/>
            <br/>
            <br/>
        </div>

    )
};
export default Content;
