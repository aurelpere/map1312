import React from "react";
import ReactDOM from "react-dom";
import {render} from "react-dom";
//import { Provider } from "react-redux";
import store from "./store/store";
import {QueryClient, QueryClientProvider} from "@tanstack/react-query"
import {ReactQueryDevtools} from "@tanstack/react-query-devtools"
import App from "./components/App";
import 'font-awesome/css/font-awesome.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../static/frontend/css/app.css';
//staleTime: time in ms before a query refetches from the server (using the cache if not)
//cacheTime: time before a query will not use cache anymore (cache will be rebuilt)
const queryClient = new QueryClient()
queryClient.setDefaultOptions({queries: {staleTime: 1000 * 60 * 10, cacheTime: 1000 * 60 * 60 * 24}},)
queryClient.setQueryDefaults('localtoken', {staleTime: 0,});
//queryClient.setQueryDefaults('remotetoken', {staleTime: 1000 * 60 * 10,});

const container = document.getElementById("app");
render(
    <React.StrictMode>
        <QueryClientProvider client={queryClient}>
            <App/>
        </QueryClientProvider>
    </React.StrictMode>,
    container
);