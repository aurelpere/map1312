from django.apps import AppConfig
import os
from django.conf import settings


class FrontendConfig(AppConfig):
    name = 'frontend'
    path = os.path.join(settings.BASE_DIR, 'frontend')
