module.exports = {
  theme: {
    opacity: {
      0: "0",
      25: ".25",
      50: ".5",
      75: ".75",
      10: ".1",
      20: ".2",
      30: ".3",
      40: ".4",
      50: ".5",
      60: ".6",
      70: ".7",
      80: ".8",
      90: ".9",
      100: "1",
    },
    boxShadow: {
      sm: "0 1px 2px 0 rgba(181, 158, 158, 0.05)",
      DEFAULT:
        "0 1px 3px 0 rgba(181, 158, 158, 0.1), 0 1px 2px 0 rgba(181, 158, 158, 0.06)",
      md: "0 4px 6px -1px rgba(181, 158, 158, 0.1), 0 2px 4px -1px rgba(181, 158, 158, 0.06)",
      lg: "0 10px 15px -3px rgba(181, 158, 158, 0.1), 0 4px 6px -2px rgba(181, 158, 158, 0.05)",
      xl: "0 20px 25px -5px rgba(181, 158, 158, 0.1), 0 10px 10px -5px rgba(181, 158, 158, 0.04)",
      "2xl": "0 25px 50px -12px rgba(181, 158, 158, 0.25)",
      "2xl-top": "0 -9px 50px -12px rgba(181, 158, 158, 0.25)",
      "2xl-blue": "0 25px 50px -12px rgba(23, 147, 248, 0.1)",
      "3xl": "0 35px 60px -15px rgba(181, 158, 158, 0.3)",
      inner: "inset 0 2px 4px 0 rgba(181, 158, 158, 0.06)",
      navResume:
        "-5px 0px 10px -7px rgba(0, 0, 0, 0.1), 5px 0px 10px -7px rgba(0, 0, 0, 0.1)",
      "blue-deck": "0 10px 20px 0px rgba(23, 88, 141, 0.1)",
      none: "none",
      darkShadow: "0px 0px 19px 10px rgba(0, 0, 0, 0.09)",
    },
    extend: {
      screens: {
        "2xl": "1536px",
        "3xl": "1920px",
        print: { raw: "print" },
      },
      lineHeight: {
        "extra-loose": "2.5",
        12: "3rem",
      },
      colors: {
        solGreen: "#8CC5A4",
        solGrener: "#74B590",
        solLightGreen: "#EEF7F2",
        solBlue: "#253746",
        solLightBlue: "#85A5D4",
        solBluer: "#333746",
        solGray: "#8F95A5",
        solDarkGray: "#999BA2",
        solLightGray: "#F3F4F7",
        solLightGray2: "#E5E7EE",
        solLightGray3: "#FAFBFC",
        solLightGray4: "#DDDEE0",
        solGrayText: "#ADADAD",
        solLightRed: "#ED9A84",
        solLightRedDarker: "#EA876F",
        solYellow: "#EDC784",
        white: "#fff",
      },

      height: {
        "1px": "1px",
      },
      fontFamily: {
        title: ["Quicksand"],
        body: ["Nunito Sans"],
      },
      fontSize: {
        "13px": "13px",
        "14px": "14px",
        "15px": "15px",
        "16px": "16px",
        "20px": "20px",
        "18px": "18px",
        "26px": "26px",
        "28px": "28px",
        "50px": "50px",
      },
      width: {
        "1px": "1px",
        68: "17rem",
        120: "120%",
        "250px": "250px",
      },
      inset: {
        moins10: "-10%",
        moins8: "-8%",
        moins6: "-6%",
        plus20: "20%",
        plus20px: "20px",
        plus100: "100%",
      },
      maxWidth: {
        md: "28rem",
        "1/5": "20%",
        "1/4": "25%",
        "1/3": "33,3333333%",
        "1/2": "50%",
        "3/4": "75%",
        "28px": "28px",
      },
      minWidth: {
        mobile: "310px",
        72: "18rem",
        80: "20rem",
      },
      minHeight: {
        12: "3rem",
        14: "3,5rem",
        16: "4rem",
      },
      zIndex: {
        "-10": "-10",
      },
      borderWidth: {
        3: "3px",
      },
    },
    spinner: (theme) => ({
      default: {
        color: "#FF9A00", // color you want to make the spinner
        size: "3em", // size of the spinner (used for both width and height)
        border: "2px", // border-width of the spinner (shouldn't be bigger than half the spinner's size)
        speed: "800ms", // the speed at which the spinner should rotate
      },
    }),
  },
  variants: {
    fill: ["hover", "focus"],
    backgroundColor: ["hover", "odd", "active"],
    extend: {
      backgroundColor: ["group-focus", "active", "checked"],
      borderColor: ["checked"],
      margin: ["last"],
      outline: ["hover", "active"],
    },
  },
  plugins: [
    require("@tailwindcss/ui"),
    require("tailwindcss-spinner")({
      className: "spinner",
      themeKey: "spinner",
    }),
    require("@tailwindcss/forms"),
  ],
  purge: {
    enabled: false,
    content: ["./src/**/*.js", "./src/**/*.html"],
  },
};
