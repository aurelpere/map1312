from django.shortcuts import render, redirect, HttpResponse
from django.views.decorators.csrf import ensure_csrf_cookie



@ensure_csrf_cookie
def index(request):
    return render(request, "frontend/index.html")
