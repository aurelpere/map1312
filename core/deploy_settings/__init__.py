from core.settings import *

RUNNING = os.environ.get("RUNNING")
dict_debug = {"prod": False, "preprod": True, "dev": True}
dict_ssl_redirect = {"prod": True, "preprod": True, "dev": False}
dict_secure_session_cookie = {"prod": True, "preprod": True, "dev": False}
dict_secure_csrf_cookie = {"prod": True, "preprod": True, "dev": False}

DEBUG = dict_debug[RUNNING]
SECURE_SSL_REDIRECT = dict_ssl_redirect[RUNNING]
SESSION_COOKIE_SECURE = dict_secure_session_cookie[RUNNING]
CSRF_COOKIE_SECURE = dict_secure_csrf_cookie[RUNNING]

TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = [
    "vpn.matangi.dev",
    "127.0.0.1",
    "192.168.3.102",
    "app-406317bc-92f9-40bd-942d-0460da4ce108.cleverapps.io",
    "jaiunshiftamontech.fun",
]

SECRET_KEY = os.environ.get("SECRET_KEY")

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": os.environ.get("POSTGRESQL_ADDON_DB", None),
        "USER": os.environ.get("POSTGRESQL_ADDON_USER", None),
        "PASSWORD": os.environ.get("POSTGRESQL_ADDON_PASSWORD", None),
        "HOST": os.environ.get("POSTGRESQL_ADDON_HOST", None),
        "PORT": os.environ.get("POSTGRESQL_ADDON_PORT", 5432),
        "OPTIONS": {
            "options": "-c statement_timeout=5000",  # Set your desired timeout in milliseconds
        },
    }
}
