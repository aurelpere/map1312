from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

front_url = [
    path('', include('map.urls')),
    path('', include('frontend.urls')),
]


urlpatterns = [
    path('wpZd2bRbpduvNcHd/', admin.site.urls),
    #path('accounts/', include('django.contrib.auth.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + front_url
