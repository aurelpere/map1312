from django.contrib.auth.backends import (
    BaseBackend,
)  # get all users authenticated list in django
from django.contrib.auth import get_user_model  # required to get user_id
from django.db.models import Q  # to make query


class Email_OR_Username(BaseBackend):
    # get user by user_id
    def get_user(self, user_id):
        try:
            return get_user_model().objects.get(pk=user_id)
        except get_user_model().DoesNotExist:
            return None

    # autentication by user by username or email
    def authenticate(self, request, username=None, password=None):
        UserModel = get_user_model()
        try:
            user = UserModel.objects.get(
                Q(email__iexact=username)
            )  # Q(username__iexact=username) | pour username
            if user.check_password(password):
                return user
        except UserModel.DoesNotExist:
            return None
