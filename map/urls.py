from django.urls import path, re_path
from . import views

urlpatterns = [
    path("map/", views.MapView.as_view(), name="map"),
    path("tiba/", views.TibaView.as_view(), name="Tiba"),
    path("nontiba/", views.NontibaView.as_view(), name="Nontiba"),
    path("id/", views.IdView.as_view(), name="id"),
    path("wgEXY2oF5AK/", views.MatricsAPIView.as_view(), name="metrics"),

]
