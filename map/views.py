from django.db import models
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from rest_framework.permissions import IsAuthenticated, AllowAny, IsAdminUser
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_protect
from django.db import connection
import re
import datetime
from .models import Metrics

class APIViewCsrf(APIView):
    @classmethod
    def as_view(cls, **initkwargs):
        """
        Store the original class on the view function.

        This allows us to discover information about the view when we do URL
        reverse lookups.  Used for breadcrumb generation.
        """
        if isinstance(getattr(cls, "queryset", None), models.query.QuerySet):
            def force_evaluation():
                raise RuntimeError(
                    "Do not evaluate the `.queryset` attribute directly, "
                    "as the result will be cached and reused between requests. "
                    "Use `.all()` or call `.get_queryset()` instead."
                )

            cls.queryset._fetch_all = force_evaluation

        view = super().as_view(**initkwargs)
        view.cls = cls
        view.initkwargs = initkwargs

        # Note: session based authentication is explicitly CSRF validated,
        # all other authentication is CSRF exempt.
        return view

    # @method_decorator(csrf_protect)
    # def dispatch(self, request, *args, **kwargs):
    #     # Try to dispatch to the right method; if a method doesn't exist,
    #     # defer to the error handler. Also defer to the error handler if the
    #     # request method isn't on the approved list.
    #     if request.method.lower() in self.http_method_names:
    #         handler = getattr(
    #             self, request.method.lower(), self.http_method_not_allowed
    #         )
    #     else:
    #         handler = self.http_method_not_allowed
    #     return handler(request, *args, **kwargs)


decorators = [never_cache, csrf_protect]


class APIViewCsrfNoCache(APIView):
    @classmethod
    def as_view(cls, **initkwargs):
        """
        Store the original class on the view function.

        This allows us to discover information about the view when we do URL
        reverse lookups.  Used for breadcrumb generation.
        """
        if isinstance(getattr(cls, "queryset", None), models.query.QuerySet):
            def force_evaluation():
                raise RuntimeError(
                    "Do not evaluate the `.queryset` attribute directly, "
                    "as the result will be cached and reused between requests. "
                    "Use `.all()` or call `.get_queryset()` instead."
                )

            cls.queryset._fetch_all = force_evaluation

        view = super().as_view(**initkwargs)
        view.cls = cls
        view.initkwargs = initkwargs

        # Note: session based authentication is explicitly CSRF validated,
        # all other authentication is CSRF exempt.
        return view

    @method_decorator(decorators)
    def dispatch(self, request, *args, **kwargs):
        # Try to dispatch to the right method; if a method doesn't exist,
        # defer to the error handler. Also defer to the error handler if the
        # request method isn't on the approved list.
        if request.method.lower() in self.http_method_names:
            handler = getattr(
                self, request.method.lower(), self.http_method_not_allowed
            )
        else:
            handler = self.http_method_not_allowed
        return handler(request, *args, **kwargs)


@method_decorator(csrf_protect, name="post")
class MapView(APIViewCsrf):
    "View de data"
    http_method_names = [
        "post"
    ]  # ['get', 'post', 'put', 'patch', 'delete', 'head', 'options', 'trace']
    authentication_classes = [SessionAuthentication]
    permission_classes = [AllowAny]
    parser_classes = [JSONParser]
    renderer_classes = [JSONRenderer]

    # serializer_class = dans GenericAPIView
    @staticmethod
    def response400():
        return Response(data={"": ""}, status=400)

    @staticmethod
    def response200():
        pass

    dictboundingbox = {1: response400, 0: response200}

    @staticmethod
    def infisnotnone(inf):
        try:
            assert isinstance(int(inf), int)
        except AssertionError as err:
            print(err)
            return 100000000000000000000
        return int(inf)

    @staticmethod
    def infisnone(inf):
        return 100000000000000000000

    dictborneinf = {1: infisnotnone, 0: infisnone}

    @staticmethod
    def supisnotnone(sup):
        try:
            assert isinstance(int(sup), int)
        except AssertionError as err:
            print(err)
            return 0
        return int(sup)

    @staticmethod
    def supisnone(sup):
        return 0

    dictbornesup = {1: supisnotnone, 0: supisnone}

    def post(self, request):
        "api de login post"
        # print(request.data)
        minlat = request.data.get("VHYJr6AeTGX", None)
        minlon = request.data.get("p3Z69bFWNWU", None)
        maxlat = request.data.get("BuMriWljfiD", None)
        maxlon = request.data.get("PFrCyMQwbJa", None)
        inf = request.data.get("inf", None)
        sup = request.data.get("sup", None)
        inf = self.dictborneinf[bool(inf is not None)](inf)
        sup = self.dictbornesup[bool(sup is not None)](sup)

        self.dictboundingbox[bool(0.3 < float(maxlat - minlat) or 0.3 < float(maxlon - minlon))]()
        boundingbox = f"""POLYGON(({minlon} {minlat},{minlon} {maxlat}, {maxlon} {maxlat},{maxlon} {minlat}, {minlon} {minlat}))"""
        with connection.cursor() as cursor:
            cursor.execute(
                f"""SELECT * from map m where ST_intersects(ST_GeometryFromText('{boundingbox}',4326),m.geom) and m.sterr_euroha>{sup} and m.sterr_euroha<{inf}""")
            data = cursor.fetchall()
            # data = json.dumps(data)
            #print(data)
        return Response(data=data, status=200)


@method_decorator(csrf_protect, name="post")
class TibaView(APIViewCsrf):
    "View de data"
    http_method_names = [
        "post"
    ]  # ['get', 'post', 'put', 'patch', 'delete', 'head', 'options', 'trace']
    authentication_classes = [SessionAuthentication]
    permission_classes = [AllowAny]
    parser_classes = [JSONParser]
    renderer_classes = [JSONRenderer]

    # serializer_class = dans GenericAPIView
    @staticmethod
    def response400():
        return Response(data={"": ""}, status=400)

    @staticmethod
    def response200():
        pass

    dictboundingbox = {1: response400, 0: response200}

    @staticmethod
    def infisnotnone(inf):
        try:
            assert isinstance(int(inf), int)
        except AssertionError as err:
            print(err)
            return 100000000000000000000
        return int(inf)

    @staticmethod
    def infisnone(inf):
        return 100000000000000000000

    dictborneinf = {1: infisnotnone, 0: infisnone}

    @staticmethod
    def supisnotnone(sup):
        try:
            assert isinstance(int(sup), int)
        except AssertionError as err:
            print(err)
            return 0
        return int(sup)

    @staticmethod
    def supisnone(sup):
        return 0

    dictbornesup = {1: supisnotnone, 0: supisnone}

    def post(self, request):
        "api de login post"
        # print(request.data)
        minlat = request.data.get("MOz7cZ5O5t9", None)
        minlon = request.data.get("CM3T01OtEMk", None)
        maxlat = request.data.get("PK7u16bdr6k", None)
        maxlon = request.data.get("ab66FO9Cofi", None)
        inf = request.data.get("inf", None)
        sup = request.data.get("sup", None)
        inf = self.dictborneinf[bool(inf is not None)](inf)
        sup = self.dictbornesup[bool(sup is not None)](sup)
        self.dictboundingbox[bool(0.3 < float(maxlat - minlat) or 0.3 < float(maxlon - minlon))]()
        boundingbox = f"""POLYGON(({minlon} {minlat},{minlon} {maxlat}, {maxlon} {maxlat},{maxlon} {minlat}, {minlon} {minlat}))"""
        with connection.cursor() as cursor:
            cursor.execute(
                f"""SELECT * from map m where ST_intersects(ST_GeometryFromText('{boundingbox}',4326),m.geom) and m.bati=true and m.sterr_euroha>{sup} and m.sterr_euroha<{inf}""")
            data = cursor.fetchall()
            # data = json.dumps(data)
            #print(data)
        return Response(data=data, status=200)


@method_decorator(csrf_protect, name="post")
class NontibaView(APIViewCsrf):
    "View de data"
    http_method_names = [
        "post"
    ]  # ['get', 'post', 'put', 'patch', 'delete', 'head', 'options', 'trace']
    authentication_classes = [SessionAuthentication]
    permission_classes = [AllowAny]
    parser_classes = [JSONParser]
    renderer_classes = [JSONRenderer]

    # serializer_class = dans GenericAPIView
    @staticmethod
    def response400():
        return Response(data={"": ""}, status=400)

    @staticmethod
    def response200():
        pass

    dictboundingbox = {1: response400, 0: response200}

    @staticmethod
    def infisnotnone(inf):
        try:
            assert isinstance(int(inf), int)
        except AssertionError as err:
            print(err)
            return 100000000000000000000
        return int(inf)

    @staticmethod
    def infisnone(inf):
        return 100000000000000000000

    dictborneinf = {1: infisnotnone, 0: infisnone}

    @staticmethod
    def supisnotnone(sup):
        try:
            assert isinstance(int(sup), int)
        except AssertionError as err:
            print(err)
            return 0
        return int(sup)

    @staticmethod
    def supisnone(sup):
        return 0

    dictbornesup = {1: supisnotnone, 0: supisnone}

    def post(self, request):
        "api de login post"
        # print(request.data)
        minlat = request.data.get("vj6Jv2xLZ6u", None)
        minlon = request.data.get("uJiGWkF75B6", None)
        maxlat = request.data.get("PWocCfzRbJT", None)
        maxlon = request.data.get("InIG2oCBgYX", None)
        inf = request.data.get("inf", None)
        sup = request.data.get("sup", None)
        inf = self.dictborneinf[bool(inf is not None)](inf)
        sup = self.dictbornesup[bool(sup is not None)](sup)
        self.dictboundingbox[bool(0.3 < float(maxlat - minlat) or 0.3 < float(maxlon - minlon))]()
        boundingbox = f"""POLYGON(({minlon} {minlat},{minlon} {maxlat}, {maxlon} {maxlat},{maxlon} {minlat}, {minlon} {minlat}))"""
        with connection.cursor() as cursor:
            cursor.execute(
                f"""SELECT * from map m where ST_intersects(ST_GeometryFromText('{boundingbox}',4326),m.geom) and m.bati=false and m.sterr_euroha>{sup} and m.sterr_euroha<{inf}""")
            data = cursor.fetchall()
            # data = json.dumps(data)
            #print(data)
        return Response(data=data, status=200)


@method_decorator(csrf_protect, name="post")
class IdView(APIViewCsrf):
    "View de data"
    http_method_names = [
        "post"
    ]  # ['get', 'post', 'put', 'patch', 'delete', 'head', 'options', 'trace']
    authentication_classes = [SessionAuthentication]
    permission_classes = [AllowAny]
    parser_classes = [JSONParser]
    renderer_classes = [JSONRenderer]

    # serializer_class = dans GenericAPIView

    def post(self, request):
        "api de login post"
        # print(request.data)
        pk = request.data.get("pk", None)
        with connection.cursor() as cursor:
            cursor.execute(
                f"""SELECT * from map m where m.id={pk}""")
            data = cursor.fetchall()
            # data = json.dumps(data)
            #print(data)
        return Response(data=data, status=200)


@method_decorator(csrf_protect, name="get")
class MatricsAPIView(APIViewCsrf):
    "view de metrics"
    authentication_classes = [SessionAuthentication]
    permission_classes = [AllowAny]
    renderer_classes = [JSONRenderer]

    def get(self, request):
        "api de login post"
        # print(request.data)
        # Dates requirements
        today = datetime.datetime.today().date()
        thismonth = datetime.datetime.today().date().month

        # uniquevisitorsday
        # Query objects that have a date within the specified range
        visitorsdayquery = Metrics.objects.filter(
            date=today,
        )
        # Use distinct() to get distinct values of the 'distinct_field' within the filtered objects
        uniquevisitorsday_distinct_values = visitorsdayquery.values_list(
            "ip", flat=True
        ).distinct()
        # Count the number of distinct values
        uniquevisitorsday_count = uniquevisitorsday_distinct_values.count()

        # uniquevistorsmonth
        monthstart = datetime.datetime.today().date().replace(day=1)
        dictmonth = {
            1: 31,
            2: 28,
            3: 31,
            4: 30,
            5: 31,
            6: 30,
            7: 31,
            8: 31,
            9: 30,
            10: 31,
            11: 30,
            12: 31,
        }
        monthend = datetime.datetime.today().date().replace(day=dictmonth[thismonth])
        visitorsmonthquery = Metrics.objects.filter(date__range=(monthstart, monthend))
        uniquevisitorsmonth_distinct_values = visitorsmonthquery.values_list(
            "ip", flat=True
        ).distinct()
        uniquevisitorsmonth_count = uniquevisitorsmonth_distinct_values.count()

        # loginday
        tibaday = visitorsdayquery.filter(path__startswith="/tiba").count()
        tibamonth = visitorsmonthquery.filter(
            path__startswith="/tiba"
        ).count()
        nontibaday = visitorsdayquery.filter(path__icontains="nontiba").count()
        nontibamonth = visitorsmonthquery.filter(
            path__icontains="nontiba"
        ).count()
        idday = visitorsdayquery.filter(
            path__icontains="id"
        ).count()
        idmonth = visitorsmonthquery.filter(
            path__icontains="id"
        ).count()
        mapday = visitorsdayquery.filter(path__icontains="map").count()
        mapmonth = visitorsmonthquery.filter(
            path__icontains="map"
        ).count()

        return Response(
            {
                "uniquevisitorsday": uniquevisitorsday_count,
                "uniquevisitorsmonth": uniquevisitorsmonth_count,
                "tibaday": tibaday,
                "tibamonth": tibamonth,
                "nontibaday": nontibaday,
                "nontibamonth": nontibamonth,
                "idday": idday,
                "idmonth": idmonth,
                "mapday": mapday,
                "mapmonth": mapmonth,
            },
            status=200,
        )
